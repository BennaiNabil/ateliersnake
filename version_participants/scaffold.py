import pygame
import random
import sys
import time
from version_participants.outils import gestion_evenements 

# Difficulté
niveaux_difficulte = {
    "facile": 10,
    "moyen": 25,
    "difficile": 40,
    "super": 60,
    "impossible": 120
}

if len(sys.argv) > 1:
    if sys.argv[1] in niveaux_difficulte:
        print("Difficulté choisie :", sys.argv[1])
        difficulty = niveaux_difficulte[sys.argv[1]]
    else:
        print(
            "La difficulté choisie n'existe pas, la difficulté par défaut est 'difficile'")
        difficulty = niveaux_difficulte["difficile"]
else:
    print("Pas de difficulté choisie, la difficulté par défaut est 'difficile'")
    difficulty = niveaux_difficulte["difficile"]

# Taille fenêtre
largeur_fenetre = 720
hauteur_fenetre = 480

# Vérification des erreurs
check_errors = pygame.init()
# pygame.init() sort un tuple de 2 nombres : (nombre de modules initialisés, nombre d'erreurs)
# Si le nombre d'erreurs est supérieur à 0, on quitte le programme
if check_errors[1] > 0:
    print(
        f'[!] Attention, il y a eu {check_errors[1]} erreurs lors de l\'initialisation du jeu, fermeture du jeu...')
    sys.exit(-1)
else:
    print('[+] Initialisation du jeu réussie !')


# Initialisation de la fenêtre
pygame.display.set_caption('Snake by Simplon')
fenetre_de_jeu = pygame.display.set_mode((largeur_fenetre, hauteur_fenetre))


# Définition des couleurs
noir = pygame.Color(0, 0, 0)
blanc = pygame.Color(255, 255, 255)
rouge = pygame.Color(255, 0, 0)
vert = pygame.Color(0, 255, 0)
bleu = pygame.Color(0, 0, 255)


# FPS (nombre de frames par seconde)
fps_controller = pygame.time.Clock()


# Variables du jeu
position_du_serpent = [100, 50]
corps_du_serpent = [[100, 50], [100-10, 50], [100-(2*10), 50]]

position_nourriture = [random.randrange(
    1, (largeur_fenetre//10)) * 10, random.randrange(1, (hauteur_fenetre//10)) * 10]
spaw_nourriture = True

direction = 'RIGHT'
virage = direction

score = 0


# Game Over
def game_over():
    police = pygame.font.SysFont('times new roman', 90)
    surface_game_over = police.render('T\'es DEAD', True, rouge)
    rectangle_game_over = surface_game_over.get_rect()
    rectangle_game_over.midtop = (largeur_fenetre/2, hauteur_fenetre/4)
    fenetre_de_jeu.fill(noir)
    fenetre_de_jeu.blit(surface_game_over, rectangle_game_over)
    affiche_score(0, rouge, 'fois', 20)
    pygame.display.flip()
    time.sleep(3)
    pygame.quit()
    sys.exit()


# Score
def affiche_score(choix, couleur, police, taille):
    score_font = pygame.font.SysFont(police, taille)
    score_surface = score_font.render('Score : ' + str(score), True, couleur)
    score_rect = score_surface.get_rect()
    if choix == 1:
        score_rect.midtop = (largeur_fenetre/10, 15)
    else:
        score_rect.midtop = (largeur_fenetre/2, hauteur_fenetre/1.25)
    fenetre_de_jeu.blit(score_surface, score_rect)
    # pygame.display.flip()


# Logique du jeu
while True:
    
    virage = gestion_evenements(virage)

    # On vérifie que le serpent ne fait pas demi-tour
    if virage == 'UP' and direction != 'DOWN':
        direction = 'UP'
    if virage == 'DOWN' and direction != 'UP':
        direction = 'DOWN'
    if virage == 'LEFT' and direction != 'RIGHT':
        direction = 'LEFT'
    if virage == 'RIGHT' and direction != 'LEFT':
        direction = 'RIGHT'

    # On déplace le serpent
    if direction == 'UP':
        position_du_serpent[1] -= 10
    if direction == 'DOWN':
        position_du_serpent[1] += 10
    if direction == 'LEFT':
        position_du_serpent[0] -= 10
    if direction == 'RIGHT':
        position_du_serpent[0] += 10

    # Mécanique de croissance du serpent
    corps_du_serpent.insert(0, list(position_du_serpent))
    if position_du_serpent[0] == position_nourriture[0] and position_du_serpent[1] == position_nourriture[1]:
        score += 1
        spaw_nourriture = False
    else:
        corps_du_serpent.pop()

    # On fait spawner la nourriture
    if not spaw_nourriture:
        position_nourriture = [random.randrange(
            1, (largeur_fenetre//10)) * 10, random.randrange(1, (hauteur_fenetre//10)) * 10]
    spaw_nourriture = True

    # Mise à jour de l'affichage
    fenetre_de_jeu.fill(noir)
    for pos in corps_du_serpent:
        # Corps du serpent
        # xy-coordinate -> .Rect(x, y, size_x, size_y)
        pygame.draw.rect(fenetre_de_jeu, vert,
                         pygame.Rect(pos[0], pos[1], 10, 10))

    # Nourriture
    pygame.draw.rect(fenetre_de_jeu, blanc, pygame.Rect(
        position_nourriture[0], position_nourriture[1], 10, 10))

    # Conditions de game over
    # On vérifie que le serpent ne touche pas les bords de la fenêtre
    if position_du_serpent[0] < 0 or position_du_serpent[0] > largeur_fenetre-10:
        game_over()
    if position_du_serpent[1] < 0 or position_du_serpent[1] > hauteur_fenetre-10:
        game_over()
    # On vérifie que le serpent ne se touche pas lui-même
    for block in corps_du_serpent[1:]:
        if position_du_serpent[0] == block[0] and position_du_serpent[1] == block[1]:
            game_over()

    affiche_score(1, blanc, 'consolas', 20)
    # Rafraichissement de l'écran
    pygame.display.update()
    # FPS
    fps_controller.tick(difficulty)
