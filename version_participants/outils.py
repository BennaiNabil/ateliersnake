import pygame


def gestion_evenements(virage):
    for evenement in pygame.event.get():
        if evenement.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        # Quand une touche est pressée
        elif evenement.type == pygame.KEYDOWN:
            # ZQSD ou flèches directionnelles
            if evenement.key == pygame.K_UP or evenement.key == ord('z'):
                virage = 'UP'
            if evenement.key == pygame.K_DOWN or evenement.key == ord('s'):
                virage = 'DOWN'
            if evenement.key == pygame.K_LEFT or evenement.key == ord('q'):
                virage = 'LEFT'
            if evenement.key == pygame.K_RIGHT or evenement.key == ord('d'):
                virage = 'RIGHT'
            # Esc -> Creation d'un évènement QUIT
            if evenement.key == pygame.K_ESCAPE:
                pygame.event.post(pygame.event.Event(pygame.QUIT))
    return virage
